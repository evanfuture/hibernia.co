<?php
/**
 * Plugin Name: Gemscape
 * Plugin URI: http://hibernia.co/plugin
 * Description: A plugin for creating and managing Gems.
 * Version: 1.0.0
 * Author: Evan Payne
 * Author URI: http://evanpayne.com
 * Text Domain: gemscape
 * Domain Path: /languages
 */

 $gemscape_includes = [
   'lib/post-types.php',
//   'lib/utils.php',           // Utility functions
//   'lib/init.php',            // Initial theme setup and constants
//   'lib/wrapper.php',         // Theme wrapper class
//   'lib/config.php',          // Configuration
//   'lib/assets.php',          // Scripts and stylesheets
//   'lib/titles.php',          // Page titles
//   'lib/nav.php',             // Custom nav modifications
//   'lib/gallery.php',         // Custom [gallery] modifications
//   'lib/extras.php',          // Custom functions
   'lib/acf-fields/acf-fields.php'
 ];

 foreach ($gemscape_includes as $file) {
   require_once $file;
 }
